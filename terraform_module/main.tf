resource "gitlab_project" "project" {
  name                                             = var.project_name
  path                                             = var.project_slug
  visibility_level                                 = var.gitlab_visibility
  description                                      = "${var.project_name} subgroup (managed by terraform)"
  default_branch                                   = "develop"
  namespace_id                                     = var.gitlab_project_location
  import_url                                       = var.template_url
  lfs_enabled                                      = false
  snippets_enabled                                 = false
  wiki_enabled                                     = false
  packages_enabled                                 = false
  container_registry_access_level                  = "disabled"
  auto_devops_enabled                              = false
  request_access_enabled                           = false
  approvals_before_merge                           = 1
  only_allow_merge_if_pipeline_succeeds            = true
  only_allow_merge_if_all_discussions_are_resolved = true
  remove_source_branch_after_merge                 = true
  merge_method                                     = "rebase_merge"

  pages_access_level          = "disabled"
  infrastructure_access_level = "private" # Required for state storage
  requirements_access_level   = "disabled"
  snippets_access_level       = "disabled"
  analytics_access_level      = "disabled"
  builds_access_level         = "private"
  releases_access_level       = "disabled"
  feature_flags_access_level  = "disabled"
  monitor_access_level        = "disabled"
  lifecycle {
    // This repository will be edited after creation, prevent accidental destroy is required.
    prevent_destroy = true
  }
}

resource "gitlab_project_variable" "GITLAB_TOKEN" {
  count   = var.no_gitlab_tokens ? 0 : 1
  masked  = true
  key     = "GITLAB_TOKEN"
  value   = var.GITLAB_TOKEN
  project = gitlab_project.project.id
}

resource "gitlab_project_variable" "GITLAB_USERNAME" {
  count   = var.no_gitlab_tokens ? 0 : 1
  masked  = true
  key     = "GITLAB_USERNAME"
  value   = var.GITLAB_USERNAME
  project = gitlab_project.project.id
}

resource "gitlab_project_variable" "SCW_SECRET_KEY" {
  count   = var.SCW_SECRET_KEY != null ? 1 : 0
  masked  = true
  key     = "SCW_SECRET_KEY"
  value   = var.SCW_SECRET_KEY
  project = gitlab_project.project.id
}

resource "gitlab_project_variable" "SCW_ACCESS_KEY" {
  count   = var.SCW_ACCESS_KEY != null ? 1 : 0
  masked  = true
  key     = "SCW_ACCESS_KEY"
  value   = var.SCW_ACCESS_KEY
  project = gitlab_project.project.id
}

locals {
  var_file = <<-EOF
    scaleway_organization_id = "${var.scaleway_organization_id}"
    dev_base_domain          = "${var.dev_base-domain}"
    prod_base_domain         = "${var.prod_base-domain}"
    project_slug             = "${var.project_slug}"
    project_name             = "${var.project_name}"

    scaleway_cluster_development_project_id = "${var.scaleway_cluster_development_project_id}"
    scaleway_cluster_development_access_key = "${var.scaleway_cluster_development_access_key}"
    scaleway_cluster_development_secret_key = "${var.scaleway_cluster_development_secret_key}"
    scaleway_cluster_development_cluster_id = "${var.scaleway_cluster_development_cluster_id}"

    scaleway_cluster_production_project_id = "${var.scaleway_cluster_production_project_id}"
    scaleway_cluster_production_access_key = "${var.scaleway_cluster_production_access_key}"
    scaleway_cluster_production_secret_key = "${var.scaleway_cluster_production_secret_key}"
    scaleway_cluster_production_cluster_id = "${var.scaleway_cluster_production_cluster_id}"

    grafana_development_url = "${try(var.grafana_development.url, "")}"
    grafana_development_api_key = "${try(var.grafana_development.api_key, "")}"
    grafana_development_org_id = "${try(var.grafana_development.org_id, "")}"
    grafana_development_loki_url = "${try(var.grafana_development.loki_url, "")}"
    grafana_development_prometheus_url = "${try(var.grafana_development.prometheus_url, "")}"

    grafana_production_url = "${try(var.grafana_production.url, "")}"
    grafana_production_api_key = "${try(var.grafana_production.api_key, "")}"
    grafana_production_org_id = "${try(var.grafana_production.org_id, "")}"
    grafana_production_loki_url = "${try(var.grafana_production.loki_url, "")}"
    grafana_production_prometheus_url = "${try(var.grafana_production.prometheus_url, "")}"
  EOF
}

resource "gitlab_project_variable" "VAR_FILE" {
  variable_type = "file"
  key           = "VAR_FILE"
  value         = local.var_file
  project       = gitlab_project.project.id
  lifecycle {
    // Only create variable, allow manual edit, without triggering redeploy from terraform
    ignore_changes = all
  }
}
