output "var_file" {
  value = local.var_file
}

output "project_id" {
  value = gitlab_project.project.id
}
