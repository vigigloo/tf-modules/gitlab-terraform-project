variable "project_name" {
  type = string

  description = "Name for the GitLab project."
}

variable "project_slug" {
  type = string

  description = "Slug for the GitLab project. Used in the project URL for example."
}

variable "gitlab_project_location" {
  type = number

  description = "ID of the GitLab group in which the GitLab project should be created."
}

variable "gitlab_visibility" {
  type    = string
  default = "public"
  validation {
    condition     = contains(["public", "private", "internal"], var.gitlab_visibility)
    error_message = "Gitlab visibility values are \"public\", \"private\", or \"internal\"."
  }

  description = "GitLab project visibility."
}

variable "template_url" {
  type    = string
  default = "https://gitlab.com/vigigloo/templates/terraform-project.git"

  description = "URL to the repository to use as template when creating the GitLab project."
}

variable "no_gitlab_tokens" {
  type    = bool
  default = false

  description = "Prevent creation of gitlab variable"
}

variable "GITLAB_TOKEN" {
  type      = string
  default   = null
  sensitive = true

  description = "Token to authenticate with GitLab's API. Needed by the GitLab terraform provider."
}

variable "GITLAB_USERNAME" {
  type      = string
  default   = null
  sensitive = true

  description = "Username to authenticate with GitLab's API. Needed by the GitLab terraform provider."
}

variable "SCW_SECRET_KEY" {
  type    = string
  default = null

  description = "Secret key to authenticate with Scaleway's API. Needed by the Scaleway terraform provider."
}

variable "SCW_ACCESS_KEY" {
  type    = string
  default = null

  description = "Access key to authenticate with Scaleway's API. Needed by the Scaleway terraform provider."
}

variable "scaleway_organization_id" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "dev_base-domain" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "prod_base-domain" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}

variable "scaleway_cluster_development_project_id" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "scaleway_cluster_development_access_key" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "scaleway_cluster_development_secret_key" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "scaleway_cluster_development_cluster_id" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "scaleway_cluster_production_project_id" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "scaleway_cluster_production_access_key" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "scaleway_cluster_production_secret_key" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}
variable "scaleway_cluster_production_cluster_id" {
  type    = string
  default = ""

  description = "Variable that will be set in the project's initial terraform var file."
}

variable "grafana_development" {
  type    = object({ url : string, api_key : string, org_id : number, loki_url : string, prometheus_url : string })
  default = null

  description = "Credentials to login to grafana (development), injected in the project's initial terraform var file"
}

variable "grafana_production" {
  type    = object({ url : string, api_key : string, org_id : number, loki_url : string, prometheus_url : string })
  default = null

  description = "Credentials to login to grafana (production), injected in the project's initial terraform var file"
}
