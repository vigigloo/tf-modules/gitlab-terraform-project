# GitLab terraform project

Creates a GitLab project for managing an infrastructure using terraform from a template.
Configures the project's CI variables to authenticate with the GitLab and Scaleway terraform providers.
The project is also configured with a default terraform var file with variables to access the production and development environments.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 15.7.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | >= 15.7.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_project.project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project) | resource |
| [gitlab_project_variable.GITLAB_TOKEN](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.GITLAB_USERNAME](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.SCW_ACCESS_KEY](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.SCW_SECRET_KEY](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.VAR_FILE](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_GITLAB_TOKEN"></a> [GITLAB\_TOKEN](#input\_GITLAB\_TOKEN) | Token to authenticate with GitLab's API. Needed by the GitLab terraform provider. | `string` | `null` | no |
| <a name="input_GITLAB_USERNAME"></a> [GITLAB\_USERNAME](#input\_GITLAB\_USERNAME) | Username to authenticate with GitLab's API. Needed by the GitLab terraform provider. | `string` | `null` | no |
| <a name="input_SCW_ACCESS_KEY"></a> [SCW\_ACCESS\_KEY](#input\_SCW\_ACCESS\_KEY) | Access key to authenticate with Scaleway's API. Needed by the Scaleway terraform provider. | `string` | `null` | no |
| <a name="input_SCW_SECRET_KEY"></a> [SCW\_SECRET\_KEY](#input\_SCW\_SECRET\_KEY) | Secret key to authenticate with Scaleway's API. Needed by the Scaleway terraform provider. | `string` | `null` | no |
| <a name="input_dev_base-domain"></a> [dev\_base-domain](#input\_dev\_base-domain) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_gitlab_project_location"></a> [gitlab\_project\_location](#input\_gitlab\_project\_location) | ID of the GitLab group in which the GitLab project should be created. | `number` | n/a | yes |
| <a name="input_gitlab_visibility"></a> [gitlab\_visibility](#input\_gitlab\_visibility) | GitLab project visibility. | `string` | `"public"` | no |
| <a name="input_grafana_development"></a> [grafana\_development](#input\_grafana\_development) | Credentials to login to grafana (development), injected in the project's initial terraform var file | `object({ url : string, api_key : string, org_id : number, loki_url : string, prometheus_url : string })` | `null` | no |
| <a name="input_grafana_production"></a> [grafana\_production](#input\_grafana\_production) | Credentials to login to grafana (production), injected in the project's initial terraform var file | `object({ url : string, api_key : string, org_id : number, loki_url : string, prometheus_url : string })` | `null` | no |
| <a name="input_no_gitlab_tokens"></a> [no\_gitlab\_tokens](#input\_no\_gitlab\_tokens) | Prevent creation of gitlab variable | `bool` | `false` | no |
| <a name="input_prod_base-domain"></a> [prod\_base-domain](#input\_prod\_base-domain) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | Name for the GitLab project. | `string` | n/a | yes |
| <a name="input_project_slug"></a> [project\_slug](#input\_project\_slug) | Slug for the GitLab project. Used in the project URL for example. | `string` | n/a | yes |
| <a name="input_scaleway_cluster_development_access_key"></a> [scaleway\_cluster\_development\_access\_key](#input\_scaleway\_cluster\_development\_access\_key) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_scaleway_cluster_development_cluster_id"></a> [scaleway\_cluster\_development\_cluster\_id](#input\_scaleway\_cluster\_development\_cluster\_id) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_scaleway_cluster_development_project_id"></a> [scaleway\_cluster\_development\_project\_id](#input\_scaleway\_cluster\_development\_project\_id) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_scaleway_cluster_development_secret_key"></a> [scaleway\_cluster\_development\_secret\_key](#input\_scaleway\_cluster\_development\_secret\_key) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_scaleway_cluster_production_access_key"></a> [scaleway\_cluster\_production\_access\_key](#input\_scaleway\_cluster\_production\_access\_key) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_scaleway_cluster_production_cluster_id"></a> [scaleway\_cluster\_production\_cluster\_id](#input\_scaleway\_cluster\_production\_cluster\_id) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_scaleway_cluster_production_project_id"></a> [scaleway\_cluster\_production\_project\_id](#input\_scaleway\_cluster\_production\_project\_id) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_scaleway_cluster_production_secret_key"></a> [scaleway\_cluster\_production\_secret\_key](#input\_scaleway\_cluster\_production\_secret\_key) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_scaleway_organization_id"></a> [scaleway\_organization\_id](#input\_scaleway\_organization\_id) | Variable that will be set in the project's initial terraform var file. | `string` | `""` | no |
| <a name="input_template_url"></a> [template\_url](#input\_template\_url) | URL to the repository to use as template when creating the GitLab project. | `string` | `"https://gitlab.com/vigigloo/templates/terraform-project.git"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_project_id"></a> [project\_id](#output\_project\_id) | n/a |
| <a name="output_var_file"></a> [var\_file](#output\_var\_file) | n/a |
<!-- END_TF_DOCS -->
